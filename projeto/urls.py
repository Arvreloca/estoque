from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include


from stock import views as stock_views

urlpatterns = [
    path('', stock_views.ProductListView.as_view(), name='product-list'),
    path('fora-de-estoque/', stock_views.OutOfStockListView.as_view(), name='out-of-stock'),
    path('adicionar-estoque/<int:pk>/',stock_views.AddStockView.as_view(), name='add-stock'),
    path('remover-estoque/<int:pk>/', stock_views.RemoveStockView.as_view(), name='remove-stock'),
    path('usuarios/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

